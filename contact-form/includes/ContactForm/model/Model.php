<?php


//model
namespace ContactForm\model;


use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class Model
{

    var $purifier;

    var $request;
    var $form;
    var $twig;
    var $validator;
    var $renderOutput;
    var $postData;
    var $lang;
    var $formIsValid;

    public function __construct()
    {
        $this->initValidator();
        $this->createForm();
        $this->register_hook_callbacks();

    }

    function initValidator()
    {
        $this->validator = Validation::createValidator();
    }

    function processForm($post_data)
    {
        $this->postData = $post_data;
        $this->form->submit($this->postData);

        if ($this->form->isValid()) {
            unset($_POST);
            $this->formIsValid = true;
        }
    }

    function createForm()
    {

        // Set up the Form component
        $formFactory = Forms::createFormFactoryBuilder()
            ->addExtension(new ValidatorExtension($this->validator))
            ->getFormFactory();

        $this->form = $formFactory
            ->createBuilder()
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'constraints' => array(
                    new NotBlank(),
                ),
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('email', TextType::class, array(
                'label' => __('Email address', 'theme-localization'),
                'constraints' => array(
                    new NotBlank(),
                    new Email(),
                ),
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('subject', TextType::class, array(
                'label' => __('Subject', 'theme-localization'),
                'constraints' => array(
                    new NotBlank(),
                ),
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('message', TextareaType::class, array(
                'label' => __('Message', 'theme-localization'),
                'constraints' => array(
                    new NotBlank(),
                ),
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => __('Submit', 'theme-localization'),
                'attr' => array('value'=>'contact-form'),
            ))

            ->getForm();
    }

    public function getForm()
    {
        return $this->form;
    }

    private function register_hook_callbacks()
    {

        //action
        add_action('init', array($this, 'init'));
        //filter

    }

    public function init()
    {


    }


}