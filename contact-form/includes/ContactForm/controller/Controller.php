<?php

namespace ContactForm\controller;


//controller

use ContactForm\model\Model;
use Symfony\Component\HttpFoundation\Request;

class Controller {

    private $model;
    private $request;

    public function __construct(Model $model) {

        $this->request = Request::createFromGlobals();
        $this->model = $model;
        $this->register_hook_callbacks();

        $this->main();

    }

    private function main()
    {
        if ($this->request->isMethod('POST')) {
            $post_data = $this->request->request->get('form');
            if( "contact-form" == $post_data['submit'] )
            {
                $this->model->processForm($post_data);
            };
        }
    }

    private function register_hook_callbacks()
    {

        //action

        //filter

    }

}
