<?php
//init hooks, front controller for queries

namespace ContactForm;

use ContactForm\controller\Controller;
use ContactForm\model\Model;
use ContactForm\view\ViewForm;
use ContactForm\view\ViewMail;
use ContactForm\view\ViewSuccess;

define('DEFAULT_FORM_THEME', 'form_div_layout.html.twig');
define('VENDOR_DIR', realpath(__DIR__ . '/../../vendor'));
define('VENDOR_FORM_DIR', VENDOR_DIR . '/symfony/form');
define('VENDOR_VALIDATOR_DIR', VENDOR_DIR . '/symfony/validator');
define('VENDOR_TWIG_BRIDGE_DIR', VENDOR_DIR . '/symfony/twig-bridge');
define('VIEWS_DIR', realpath(__DIR__ . '/view/templates'));

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );


//front controller
class FrontController  {

    var $model;

    function __construct() {
        $this->register_hook_callbacks();
    }

    private function register_hook_callbacks()
    {

        //filter
        add_filter( 'query_vars', array( $this,'cc_contactform_add_query_var') );
        //action
        add_action('init', array($this, 'init'));
        add_action( 'template_redirect', array( $this,'cc_contactform_template_redirect') );


    }

    public function init()
    {
        $this->cc_contactform_set_custom_rewrite_rules();

        //model
        $this->model = new Model();

        //controller
        $controller = new Controller($this->model);

        if($this->model->formIsValid)
        {
            $this->sendMail();
            $this->redirect();
        }
        else
        {
            $view = new ViewForm($this->model);
        }
    }

    function redirect()
    {
        //redirect
        $queryArg = add_query_arg('response', 'success');
        header('Location: ' . $queryArg);
    }

    function sendMail()
    {

        global $blog_id;
        $sendToArray = array();

        $sendToArray[] = "test@localhost.com";
        $transport = \Swift_SmtpTransport::newInstance('localhost', 1025);

        // Create the Mailer using your created Transport
        $mailer = \Swift_Mailer::newInstance($transport);

        // Create the message
        //$post_data = $this->request->request->get($this->form->getName());

        $view = new ViewMail($this->model);

        $body = $view->render();

        $message = \Swift_Message::newInstance()
            // Give the message a subject
            ->setSubject($this->model->postData['subject'])
            // Set the From address with an associative array
            ->setFrom(array($this->model->postData['email'] => $this->model->postData['name']))
            // Set the To addresses with an associative array
            ->setTo($sendToArray)
            // Give it a body
            ->setBody($body, 'text/plain');

        // Send the message
        $result = $mailer->send($message);

    }

    public function cc_contactform_template_redirect() {

        //match get and post requests
        if ('success' == get_query_var('response')) {

            $view = new ViewSuccess($this->model);

        }
        if ( 'hello' === get_query_var( 'action' ) ) {

            $this->model = new Model(get_query_var( 'name' ));
            $controller = new Controller($this->model);
            $view = new View($this->model);

            $output = $view->render();
            echo $output;
            //wp_redirect( wp_get_referer() );
            exit;

        }
        else
        {
            return;
        }
    }

    /*
     *
     * • On plugin activation, add the rule and flush.
        • On init, also add the rule, in case another plugin
        flushes the rules.
        • Don’t flush rules on every page request (for example
        hooking in init); that would cause unnecessary
        overhead.
        • On plugin deactivati  on, flush the rules again to clear
        the list.
     */
    public static function cc_contactform_set_custom_rewrite_rules()
    {
        //match get requests
        //add_rewrite_rule( 'hello/([^/]+)(?:/([0-9]+))?/?$',
        //    'index.php?action=hello&name=$matches[1]',
        //    'top' );
        //flush_rewrite_rules();

    }

    public function cc_contactform_add_query_var($vars)
    {
        $vars[] = "response";
        return $vars;
    }

    public static function cc_contactform_activation() {
        FrontController::cc_contactform_set_custom_rewrite_rules();
        flush_rewrite_rules();
    }

}