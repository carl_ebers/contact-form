<?php

namespace ContactForm\view;

//view


use ContactForm\model\Model;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validation;


class ViewSuccess  {

    private $model;
    private $output;
    private $twig;
    var $lang;

    public function __construct(Model $model) {

        $this->initTwig();
        $this->model = $model;
        $this->register_hook_callbacks();
        $this->init();

    }

    public function register_hook_callbacks()
    {

        //action

        //filter

    }


    private function initTwig()
    {

        // Set up the Translation component
        if ( 'de' == $this->lang )
        {
            $translator = new Translator('de');
        }
        else
        {
            $translator = new Translator('en');
        }

        $translator->addLoader('xlf', new XliffFileLoader());
        $translator->addResource('xlf', VENDOR_FORM_DIR . '/Resources/translations/validators.de.xlf', 'de');
        $translator->addResource('xlf', VENDOR_VALIDATOR_DIR . '/Resources/translations/validators.de.xlf', 'de');

        $validator = Validation::createValidator();

        $this->twig = new \Twig_Environment(new \Twig_Loader_Filesystem(array(
            VIEWS_DIR,
            VENDOR_TWIG_BRIDGE_DIR . '/Resources/views/Form',
        )));

        $formEngine = new TwigRendererEngine(array(DEFAULT_FORM_THEME));
        $formEngine->setEnvironment($this->twig);
        $this->twig->addExtension(new TranslationExtension($translator));
        $this->twig->addExtension(
            new FormExtension(new TwigRenderer($formEngine))
        );
    }

    public function init()
    {
        add_shortcode('cceContactForm', array($this, 'getShortCodeOutput'));
    }

    public function getShortCodeOutput()
    {
        $textArray = array(
            'headline' => __('Thank you for your message', 'theme-localization'),
            'subline'  => __('We’ll try and get back to you as soon as possible', 'theme-localization'),
            'linkText'  =>  __('Send another message', 'theme-localization')
        );

        $this->output = $this->twig->render('answer.html.twig', $textArray );

        return $this->output;
    }

    public function render()
    {


    }

}