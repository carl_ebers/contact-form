<?php
/*
Plugin Name: Contact Form
Plugin URI:
Description: Contact Form
Version:     1.0
Author:      Carl-Constantin Ebers
Author URI:
License: GPLv2
*/



if ( ! defined( 'ABSPATH' ) ) {
	die( 'Access denied.' );
}
require __DIR__ . '/vendor/autoload.php';


register_activation_hook( __FILE__, '\ContactForm\FrontController::cc_contactform_activation');
$frontController = new \ContactForm\FrontController();
